package nl.robertvankammen.blockshuffle;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class PlayerScore implements ConfigurationSerializable {
    static {
        ConfigurationSerialization.registerClass(PlayerScore.class);
    }

    private String player;
    private Integer score;

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("player", player);
        data.put("score", score);
        return data;
    }

    public PlayerScore(String player, Integer score) {
        this.player = player;
        this.score = score;
    }

    public void addOneToScore() {
        this.score++;
    }

    public PlayerScore(Map<String, Object> data) {
        player = (String) data.get("player");
        score = (Integer) data.get("score");
    }

    public int compareTo(PlayerScore playerScore) {
        return playerScore.getScore() - this.score;
    }
}