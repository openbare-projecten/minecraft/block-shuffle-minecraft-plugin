package nl.robertvankammen.blockshuffle;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import lombok.val;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Objective;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static net.md_5.bungee.api.ChatColor.translateAlternateColorCodes;
import static org.bukkit.Bukkit.*;
import static org.bukkit.scoreboard.DisplaySlot.SIDEBAR;

// TODO block 'jatten' in de laatste minuut
public class Main extends JavaPlugin implements Listener {

    private static final List<Material> BLOCKS_TO_EASY = new ArrayList<>();
    private static final List<Material> BLOCKS_EASY = new ArrayList<>();
    private static final List<Material> BLOCKS_NORMAL = new ArrayList<>();
    private static final List<Material> BLOCKS_HARD = new ArrayList<>();
    private static final Integer TICKS_TO_MINUTES = 20 * 60;
    private static final String TO_EASY = "to_easy";
    private static final String EASY = "easy";
    private static final String NORMAL = "normal";
    private static final String HARD = "hard";

    static {
        BLOCKS_TO_EASY.add(Material.STONE);
        BLOCKS_TO_EASY.add(Material.STONE_SLAB);
        BLOCKS_TO_EASY.add(Material.STONE_STAIRS);
        BLOCKS_TO_EASY.add(Material.STONE_BRICKS);
        BLOCKS_TO_EASY.add(Material.SMOOTH_STONE);
        BLOCKS_TO_EASY.add(Material.SMOOTH_STONE_SLAB);

        BLOCKS_TO_EASY.add(Material.GRANITE);
        BLOCKS_TO_EASY.add(Material.GRANITE_SLAB);
        BLOCKS_TO_EASY.add(Material.GRANITE_WALL);
        BLOCKS_TO_EASY.add(Material.POLISHED_GRANITE);
        BLOCKS_TO_EASY.add(Material.POLISHED_GRANITE_STAIRS);
        BLOCKS_TO_EASY.add(Material.POLISHED_GRANITE_SLAB);

        BLOCKS_TO_EASY.add(Material.DIORITE);
        BLOCKS_TO_EASY.add(Material.DIORITE_SLAB);
        BLOCKS_TO_EASY.add(Material.DIORITE_WALL);
        BLOCKS_TO_EASY.add(Material.POLISHED_DIORITE);
        BLOCKS_TO_EASY.add(Material.POLISHED_DIORITE_STAIRS);
        BLOCKS_TO_EASY.add(Material.POLISHED_DIORITE_SLAB);

        BLOCKS_TO_EASY.add(Material.ANDESITE);
        BLOCKS_TO_EASY.add(Material.ANDESITE_SLAB);
        BLOCKS_TO_EASY.add(Material.ANDESITE_WALL);
        BLOCKS_TO_EASY.add(Material.POLISHED_ANDESITE);
        BLOCKS_TO_EASY.add(Material.POLISHED_ANDESITE_STAIRS);
        BLOCKS_TO_EASY.add(Material.POLISHED_ANDESITE_SLAB);

        BLOCKS_TO_EASY.add(Material.GRASS_BLOCK);
        BLOCKS_TO_EASY.add(Material.DIRT);

        BLOCKS_TO_EASY.add(Material.COBBLESTONE);
        BLOCKS_TO_EASY.add(Material.COBBLESTONE_SLAB);
        BLOCKS_TO_EASY.add(Material.COBBLESTONE_STAIRS);
        BLOCKS_TO_EASY.add(Material.COBBLESTONE_WALL);

        BLOCKS_TO_EASY.add(Material.OAK_LEAVES);
        BLOCKS_TO_EASY.add(Material.OAK_LOG);
        BLOCKS_TO_EASY.add(Material.OAK_PLANKS);
        BLOCKS_TO_EASY.add(Material.OAK_STAIRS);
        BLOCKS_TO_EASY.add(Material.OAK_TRAPDOOR);

        BLOCKS_TO_EASY.add(Material.SPRUCE_LEAVES);
        BLOCKS_TO_EASY.add(Material.SPRUCE_LOG);
        BLOCKS_TO_EASY.add(Material.SPRUCE_PLANKS);
        BLOCKS_TO_EASY.add(Material.SPRUCE_STAIRS);
        BLOCKS_TO_EASY.add(Material.SPRUCE_TRAPDOOR);

        BLOCKS_TO_EASY.add(Material.BIRCH_LEAVES);
        BLOCKS_TO_EASY.add(Material.BIRCH_LOG);
        BLOCKS_TO_EASY.add(Material.BIRCH_PLANKS);
        BLOCKS_TO_EASY.add(Material.BIRCH_STAIRS);
        BLOCKS_TO_EASY.add(Material.BIRCH_TRAPDOOR);

        BLOCKS_TO_EASY.add(Material.DARK_OAK_LEAVES);
        BLOCKS_TO_EASY.add(Material.DARK_OAK_LOG);
        BLOCKS_TO_EASY.add(Material.DARK_OAK_PLANKS);
        BLOCKS_TO_EASY.add(Material.DARK_OAK_STAIRS);
        BLOCKS_TO_EASY.add(Material.DARK_OAK_TRAPDOOR);

        BLOCKS_TO_EASY.add(Material.BEDROCK);
        BLOCKS_TO_EASY.add(Material.SAND);
        BLOCKS_TO_EASY.add(Material.GRAVEL);

        BLOCKS_TO_EASY.add(Material.GOLD_ORE);
        BLOCKS_TO_EASY.add(Material.IRON_ORE);
        BLOCKS_TO_EASY.add(Material.COAL_ORE);

        BLOCKS_TO_EASY.add(Material.GLASS);
        BLOCKS_TO_EASY.add(Material.OBSIDIAN);
        BLOCKS_TO_EASY.add(Material.CRYING_OBSIDIAN);
        BLOCKS_TO_EASY.add(Material.BRICKS);
        BLOCKS_TO_EASY.add(Material.BRICK_STAIRS);
        BLOCKS_TO_EASY.add(Material.BRICK_WALL);
        BLOCKS_TO_EASY.add(Material.PUMPKIN);

        BLOCKS_TO_EASY.add(Material.HAY_BLOCK);
        BLOCKS_TO_EASY.add(Material.TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.COAL_BLOCK);

        BLOCKS_TO_EASY.add(Material.WHITE_WOOL);
        BLOCKS_TO_EASY.add(Material.ORANGE_WOOL);
        BLOCKS_TO_EASY.add(Material.MAGENTA_WOOL);
        BLOCKS_TO_EASY.add(Material.YELLOW_WOOL);
        BLOCKS_TO_EASY.add(Material.PINK_WOOL);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_WOOL);
        BLOCKS_TO_EASY.add(Material.PURPLE_WOOL);
        BLOCKS_TO_EASY.add(Material.BLUE_WOOL);
        BLOCKS_TO_EASY.add(Material.RED_WOOL);
        BLOCKS_TO_EASY.add(Material.BLACK_WOOL);

        BLOCKS_TO_EASY.add(Material.WHITE_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.ORANGE_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.MAGENTA_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.YELLOW_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.PINK_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.PURPLE_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.BLUE_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.RED_STAINED_GLASS);
        BLOCKS_TO_EASY.add(Material.BLACK_STAINED_GLASS);

        BLOCKS_TO_EASY.add(Material.WHITE_CONCRETE);
        BLOCKS_TO_EASY.add(Material.ORANGE_CONCRETE);
        BLOCKS_TO_EASY.add(Material.MAGENTA_CONCRETE);
        BLOCKS_TO_EASY.add(Material.YELLOW_CONCRETE);
        BLOCKS_TO_EASY.add(Material.PINK_CONCRETE);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_CONCRETE);
        BLOCKS_TO_EASY.add(Material.PURPLE_CONCRETE);
        BLOCKS_TO_EASY.add(Material.BLUE_CONCRETE);
        BLOCKS_TO_EASY.add(Material.RED_CONCRETE);
        BLOCKS_TO_EASY.add(Material.BLACK_CONCRETE);

        BLOCKS_TO_EASY.add(Material.WHITE_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.ORANGE_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.MAGENTA_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.YELLOW_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.PINK_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.PURPLE_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.BLUE_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.RED_CONCRETE_POWDER);
        BLOCKS_TO_EASY.add(Material.BLACK_CONCRETE_POWDER);

        BLOCKS_TO_EASY.add(Material.WHITE_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.ORANGE_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.MAGENTA_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.YELLOW_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.PINK_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.PURPLE_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.BLUE_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.RED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.BLACK_TERRACOTTA);

        BLOCKS_TO_EASY.add(Material.WHITE_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.ORANGE_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.MAGENTA_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.YELLOW_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.PINK_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.PURPLE_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.BLUE_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.RED_GLAZED_TERRACOTTA);
        BLOCKS_TO_EASY.add(Material.BLACK_GLAZED_TERRACOTTA);

        BLOCKS_TO_EASY.add(Material.WHITE_BED);
        BLOCKS_TO_EASY.add(Material.ORANGE_BED);
        BLOCKS_TO_EASY.add(Material.MAGENTA_BED);
        BLOCKS_TO_EASY.add(Material.YELLOW_BED);
        BLOCKS_TO_EASY.add(Material.PINK_BED);
        BLOCKS_TO_EASY.add(Material.LIGHT_GRAY_BED);
        BLOCKS_TO_EASY.add(Material.PURPLE_BED);
        BLOCKS_TO_EASY.add(Material.BLUE_BED);
        BLOCKS_TO_EASY.add(Material.BLACK_BED);
        BLOCKS_TO_EASY.add(Material.RED_BED);

        BLOCKS_TO_EASY.add(Material.HOPPER);
        BLOCKS_TO_EASY.add(Material.RAIL);

        BLOCKS_TO_EASY.add(Material.LOOM);
        BLOCKS_TO_EASY.add(Material.COMPOSTER);
        BLOCKS_TO_EASY.add(Material.BARREL);
        BLOCKS_TO_EASY.add(Material.SMOKER);
        BLOCKS_TO_EASY.add(Material.BLAST_FURNACE);
        BLOCKS_TO_EASY.add(Material.CARTOGRAPHY_TABLE);
        BLOCKS_TO_EASY.add(Material.FLETCHING_TABLE);
        BLOCKS_TO_EASY.add(Material.GRINDSTONE);
        BLOCKS_TO_EASY.add(Material.SMITHING_TABLE);
        BLOCKS_TO_EASY.add(Material.STONECUTTER);
        BLOCKS_TO_EASY.add(Material.BELL);
        BLOCKS_TO_EASY.add(Material.CAMPFIRE);
        BLOCKS_TO_EASY.add(Material.BOOKSHELF);


        BLOCKS_EASY.add(Material.LIGHT_BLUE_WOOL);
        BLOCKS_EASY.add(Material.GRAY_WOOL);

        BLOCKS_EASY.add(Material.LIGHT_BLUE_STAINED_GLASS);
        BLOCKS_EASY.add(Material.GRAY_STAINED_GLASS);

        BLOCKS_EASY.add(Material.LIGHT_BLUE_CONCRETE);
        BLOCKS_EASY.add(Material.GRAY_CONCRETE);

        BLOCKS_EASY.add(Material.LIGHT_BLUE_CONCRETE_POWDER);
        BLOCKS_EASY.add(Material.GRAY_CONCRETE_POWDER);

        BLOCKS_EASY.add(Material.LIGHT_BLUE_TERRACOTTA);
        BLOCKS_EASY.add(Material.GRAY_TERRACOTTA);

        BLOCKS_EASY.add(Material.LIGHT_BLUE_GLAZED_TERRACOTTA);
        BLOCKS_EASY.add(Material.GRAY_GLAZED_TERRACOTTA);

        BLOCKS_EASY.add(Material.LIGHT_BLUE_BED);
        BLOCKS_EASY.add(Material.GRAY_BED);

        BLOCKS_EASY.add(Material.ACACIA_LEAVES);
        BLOCKS_EASY.add(Material.ACACIA_LOG);
        BLOCKS_EASY.add(Material.ACACIA_PLANKS);
        BLOCKS_EASY.add(Material.ACACIA_STAIRS);
        BLOCKS_EASY.add(Material.ACACIA_TRAPDOOR);

        BLOCKS_EASY.add(Material.ACACIA_WOOD);
        BLOCKS_EASY.add(Material.BIRCH_WOOD);
        BLOCKS_EASY.add(Material.SPRUCE_WOOD);
        BLOCKS_EASY.add(Material.DARK_OAK_WOOD);
        BLOCKS_EASY.add(Material.OAK_WOOD);

        BLOCKS_EASY.add(Material.STRIPPED_ACACIA_WOOD);
        BLOCKS_EASY.add(Material.STRIPPED_BIRCH_WOOD);
        BLOCKS_EASY.add(Material.STRIPPED_SPRUCE_WOOD);
        BLOCKS_EASY.add(Material.STRIPPED_DARK_OAK_WOOD);
        BLOCKS_EASY.add(Material.STRIPPED_OAK_WOOD);

        BLOCKS_EASY.add(Material.SANDSTONE);
        BLOCKS_EASY.add(Material.SANDSTONE_SLAB);
        BLOCKS_EASY.add(Material.SMOOTH_SANDSTONE);
        BLOCKS_EASY.add(Material.CHISELED_SANDSTONE);

        BLOCKS_EASY.add(Material.LILY_PAD);
        BLOCKS_EASY.add(Material.POWERED_RAIL);
        BLOCKS_EASY.add(Material.BROWN_MUSHROOM_BLOCK);
        BLOCKS_EASY.add(Material.RED_MUSHROOM_BLOCK);
        BLOCKS_EASY.add(Material.MUSHROOM_STEM);
        BLOCKS_EASY.add(Material.CARVED_PUMPKIN);
        BLOCKS_EASY.add(Material.JACK_O_LANTERN);
        BLOCKS_EASY.add(Material.CRACKED_STONE_BRICKS);


        BLOCKS_NORMAL.add(Material.DIAMOND_ORE);
        BLOCKS_NORMAL.add(Material.LAPIS_ORE);
        BLOCKS_NORMAL.add(Material.REDSTONE_ORE);

        BLOCKS_NORMAL.add(Material.GOLD_BLOCK);
        BLOCKS_NORMAL.add(Material.IRON_BLOCK);
        BLOCKS_NORMAL.add(Material.IRON_TRAPDOOR);

        BLOCKS_NORMAL.add(Material.NETHERRACK);
        BLOCKS_NORMAL.add(Material.SOUL_SAND);
        BLOCKS_NORMAL.add(Material.SOUL_SOIL);

        BLOCKS_NORMAL.add(Material.BASALT);
        BLOCKS_NORMAL.add(Material.POLISHED_BASALT);

        BLOCKS_NORMAL.add(Material.BLACKSTONE);
        BLOCKS_NORMAL.add(Material.POLISHED_BLACKSTONE);
        BLOCKS_NORMAL.add(Material.BLACKSTONE_WALL);
        BLOCKS_NORMAL.add(Material.CHISELED_POLISHED_BLACKSTONE);
        BLOCKS_NORMAL.add(Material.CRACKED_POLISHED_BLACKSTONE_BRICKS);

        BLOCKS_NORMAL.add(Material.QUARTZ_STAIRS);
        BLOCKS_NORMAL.add(Material.SMOOTH_QUARTZ);
        BLOCKS_NORMAL.add(Material.QUARTZ_PILLAR);
        BLOCKS_NORMAL.add(Material.QUARTZ_BLOCK);

        BLOCKS_NORMAL.add(Material.NETHER_BRICKS);
        BLOCKS_NORMAL.add(Material.NETHER_BRICK_STAIRS);
        BLOCKS_NORMAL.add(Material.NETHER_BRICK_SLAB);
        BLOCKS_NORMAL.add(Material.NETHER_BRICK_WALL);
        BLOCKS_NORMAL.add(Material.NETHER_BRICK_FENCE);

        BLOCKS_NORMAL.add(Material.REDSTONE_LAMP);
        BLOCKS_NORMAL.add(Material.NOTE_BLOCK);
        BLOCKS_NORMAL.add(Material.TNT);
        BLOCKS_NORMAL.add(Material.PISTON);
        BLOCKS_NORMAL.add(Material.DROPPER);
        BLOCKS_NORMAL.add(Material.LANTERN);
        BLOCKS_NORMAL.add(Material.CHAIN);

        BLOCKS_NORMAL.add(Material.MELON);

        BLOCKS_NORMAL.add(Material.CYAN_WOOL);
        BLOCKS_NORMAL.add(Material.BROWN_WOOL);
        BLOCKS_NORMAL.add(Material.LIME_WOOL);
        BLOCKS_NORMAL.add(Material.GREEN_WOOL);

        BLOCKS_NORMAL.add(Material.CYAN_STAINED_GLASS);
        BLOCKS_NORMAL.add(Material.BROWN_STAINED_GLASS);
        BLOCKS_NORMAL.add(Material.LIME_STAINED_GLASS);
        BLOCKS_NORMAL.add(Material.GREEN_STAINED_GLASS);

        BLOCKS_NORMAL.add(Material.CYAN_CONCRETE);
        BLOCKS_NORMAL.add(Material.BROWN_CONCRETE);
        BLOCKS_NORMAL.add(Material.LIME_CONCRETE);
        BLOCKS_NORMAL.add(Material.GREEN_CONCRETE);

        BLOCKS_NORMAL.add(Material.CYAN_CONCRETE_POWDER);
        BLOCKS_NORMAL.add(Material.BROWN_CONCRETE_POWDER);
        BLOCKS_NORMAL.add(Material.LIME_CONCRETE_POWDER);
        BLOCKS_NORMAL.add(Material.GREEN_CONCRETE_POWDER);

        BLOCKS_NORMAL.add(Material.CYAN_TERRACOTTA);
        BLOCKS_NORMAL.add(Material.BROWN_TERRACOTTA);
        BLOCKS_NORMAL.add(Material.LIME_TERRACOTTA);
        BLOCKS_NORMAL.add(Material.GREEN_TERRACOTTA);

        BLOCKS_NORMAL.add(Material.CYAN_GLAZED_TERRACOTTA);
        BLOCKS_NORMAL.add(Material.BROWN_GLAZED_TERRACOTTA);
        BLOCKS_NORMAL.add(Material.LIME_GLAZED_TERRACOTTA);
        BLOCKS_NORMAL.add(Material.GREEN_GLAZED_TERRACOTTA);

        BLOCKS_NORMAL.add(Material.CYAN_BED);
        BLOCKS_NORMAL.add(Material.BROWN_BED);
        BLOCKS_NORMAL.add(Material.LIME_BED);
        BLOCKS_NORMAL.add(Material.GREEN_BED);

        BLOCKS_NORMAL.add(Material.JUNGLE_LEAVES);
        BLOCKS_NORMAL.add(Material.JUNGLE_LOG);
        BLOCKS_NORMAL.add(Material.JUNGLE_PLANKS);
        BLOCKS_NORMAL.add(Material.JUNGLE_STAIRS);
        BLOCKS_NORMAL.add(Material.JUNGLE_TRAPDOOR);
        BLOCKS_NORMAL.add(Material.JUNGLE_WOOD);
        BLOCKS_NORMAL.add(Material.STRIPPED_JUNGLE_WOOD);

        BLOCKS_NORMAL.add(Material.PRISMARINE_BRICKS);
        BLOCKS_NORMAL.add(Material.PRISMARINE);
        BLOCKS_NORMAL.add(Material.PRISMARINE_WALL);
        BLOCKS_NORMAL.add(Material.DARK_PRISMARINE);
        BLOCKS_NORMAL.add(Material.SEA_LANTERN);

        BLOCKS_NORMAL.add(Material.MOSSY_STONE_BRICKS);
        BLOCKS_NORMAL.add(Material.MOSSY_COBBLESTONE);
        BLOCKS_NORMAL.add(Material.MOSSY_COBBLESTONE_WALL);

        BLOCKS_NORMAL.add(Material.COARSE_DIRT);
        BLOCKS_NORMAL.add(Material.PODZOL);


        BLOCKS_HARD.add(Material.LAPIS_BLOCK);
        BLOCKS_HARD.add(Material.REDSTONE_BLOCK);
        BLOCKS_HARD.add(Material.EMERALD_ORE);
        BLOCKS_HARD.add(Material.NETHER_GOLD_ORE);
        BLOCKS_HARD.add(Material.NETHER_QUARTZ_ORE);
        BLOCKS_HARD.add(Material.WET_SPONGE);
        BLOCKS_HARD.add(Material.DISPENSER);
        BLOCKS_HARD.add(Material.TARGET);
        BLOCKS_HARD.add(Material.DAYLIGHT_DETECTOR);
        BLOCKS_HARD.add(Material.STICKY_PISTON);
        BLOCKS_HARD.add(Material.WARPED_STEM);
        BLOCKS_HARD.add(Material.WARPED_HYPHAE);
        BLOCKS_HARD.add(Material.WARPED_PLANKS);
        BLOCKS_HARD.add(Material.WARPED_STAIRS);
        BLOCKS_HARD.add(Material.WARPED_TRAPDOOR);
        BLOCKS_HARD.add(Material.SOUL_CAMPFIRE);
        BLOCKS_HARD.add(Material.ANCIENT_DEBRIS);
        BLOCKS_HARD.add(Material.RED_SANDSTONE);
        BLOCKS_HARD.add(Material.RED_SAND);
        BLOCKS_HARD.add(Material.CRIMSON_TRAPDOOR);
        BLOCKS_HARD.add(Material.CRIMSON_STEM);
        BLOCKS_HARD.add(Material.CRIMSON_HYPHAE);
        BLOCKS_HARD.add(Material.CRIMSON_PLANKS);
        BLOCKS_HARD.add(Material.CRIMSON_STAIRS);
        BLOCKS_HARD.add(Material.SOUL_LANTERN);
        BLOCKS_HARD.add(Material.NETHER_WART_BLOCK);
        BLOCKS_HARD.add(Material.SMOOTH_RED_SANDSTONE);
        BLOCKS_HARD.add(Material.RED_SANDSTONE_SLAB);
        BLOCKS_HARD.add(Material.CUT_RED_SANDSTONE_SLAB);
        BLOCKS_HARD.add(Material.WARPED_WART_BLOCK);
        BLOCKS_HARD.add(Material.RED_NETHER_BRICKS);
        BLOCKS_HARD.add(Material.CAKE);
        BLOCKS_HARD.add(Material.RED_NETHER_BRICK_WALL);
        BLOCKS_HARD.add(Material.WARPED_NYLIUM);
        BLOCKS_HARD.add(Material.CRIMSON_NYLIUM);
        BLOCKS_HARD.add(Material.SLIME_BLOCK);
        BLOCKS_HARD.add(Material.ENCHANTING_TABLE);
        BLOCKS_HARD.add(Material.RED_NETHER_BRICK_STAIRS);
        BLOCKS_HARD.add(Material.RED_NETHER_BRICK_SLAB);
        BLOCKS_HARD.add(Material.RED_NETHER_BRICK_WALL);
    }

    private final List<Material> chosedBlocks = new ArrayList<>();
    private final Map<Player, String> playersBlocks = new HashMap<>();
    private final List<Player> activePlayers = new ArrayList<>();

    private boolean gameActive = false;
    private boolean stopGame = false;
    private String currentMode = NORMAL;
    private LocalDateTime endTimeDate = LocalDateTime.now();
    private long minutes = 5L;

    private int taskId;
    private int taskId2;
    private int taskIdPlayerScoreboard;

    private Hologram hologramToEasyBlocks;
    private Hologram hologramToEasyWins;
    private Hologram hologramEasyWins;
    private Hologram hologramEasyBlocks;
    private Hologram hologramNormalWins;
    private Hologram hologramNormalBlocks;
    private Hologram hologramHardWins;
    private Hologram hologramHardBlocks;

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);

        initScoreboard();

        updateAllHologrameOfGamemode(TO_EASY);
        updateAllHologrameOfGamemode(EASY);
        updateAllHologrameOfGamemode(NORMAL);
        updateAllHologrameOfGamemode(HARD);
    }

    private void initScoreboard() {
        var world = getServer().getWorlds().get(0);
        var locationToEasyBlocks = new Location(world, 165, 70, 153.5);
        var locationToEasyWins = new Location(world, 165, 70, 159.5);
        var locationEasyWins = new Location(world, 149.5, 70, 172);
        var locationEasyBlocks = new Location(world, 143.5, 70, 172);
        var locationNormalWins = new Location(world, 162, 70, 187.5);
        var locationNormalBlocks = new Location(world, 162, 70, 193.5);
        var locationHardWins = new Location(world, 177.5, 70, 175);
        var locationHardBlocks = new Location(world, 183.5, 70, 175);

        hologramToEasyBlocks = HologramsAPI.createHologram(this, locationToEasyBlocks);
        hologramToEasyWins = HologramsAPI.createHologram(this, locationToEasyWins);
        hologramEasyWins = HologramsAPI.createHologram(this, locationEasyWins);
        hologramEasyBlocks = HologramsAPI.createHologram(this, locationEasyBlocks);
        hologramNormalWins = HologramsAPI.createHologram(this, locationNormalWins);
        hologramNormalBlocks = HologramsAPI.createHologram(this, locationNormalBlocks);
        hologramHardWins = HologramsAPI.createHologram(this, locationHardWins);
        hologramHardBlocks = HologramsAPI.createHologram(this, locationHardBlocks);

        hologramToEasyBlocks.insertTextLine(0, "---------------");
        hologramToEasyBlocks.insertTextLine(1, ChatColor.GREEN + "Blocks found");
        hologramToEasyWins.insertTextLine(0, "---------------");
        hologramToEasyWins.insertTextLine(1, ChatColor.GREEN + "Times won");
        hologramEasyWins.insertTextLine(0, "---------------");
        hologramEasyWins.insertTextLine(1, ChatColor.YELLOW + "Times won");
        hologramEasyBlocks.insertTextLine(0, "---------------");
        hologramEasyBlocks.insertTextLine(1, ChatColor.YELLOW + "Blocks found");
        hologramNormalWins.insertTextLine(0, "---------------");
        hologramNormalWins.insertTextLine(1, ChatColor.GOLD + "Times won");
        hologramNormalBlocks.insertTextLine(0, "---------------");
        hologramNormalBlocks.insertTextLine(1, ChatColor.GOLD + "Blocks found");
        hologramHardWins.insertTextLine(0, "---------------");
        hologramHardWins.insertTextLine(1, ChatColor.RED + "Times won");
        hologramHardBlocks.insertTextLine(0, "---------------");
        hologramHardBlocks.insertTextLine(1, ChatColor.RED + "Blocks found");
    }

    private void updateAllHologrameOfGamemode(String gamemode) {
        Hologram hologramBlocks = null;
        Hologram hologramWins = null;
        ChatColor color = null;
        switch (gamemode) {
            case TO_EASY:
                hologramBlocks = hologramToEasyBlocks;
                hologramWins = hologramToEasyWins;
                color = ChatColor.GREEN;
                break;
            case EASY:
                hologramBlocks = hologramEasyBlocks;
                hologramWins = hologramEasyWins;
                color = ChatColor.YELLOW;
                break;
            case NORMAL:
                hologramBlocks = hologramNormalBlocks;
                hologramWins = hologramNormalWins;
                color = ChatColor.GOLD;
                break;
            case HARD:
                hologramBlocks = hologramHardBlocks;
                hologramWins = hologramHardWins;
                color = ChatColor.RED;
                break;
        }
        updateSpecifikeHologram(hologramBlocks, gamemode, "blocks", color);
        updateSpecifikeHologram(hologramWins, gamemode, "wins", color);
    }

    private void updateSpecifikeHologram(Hologram hologram, String gameMode, String type, ChatColor color) {
        var count = new AtomicInteger(0);
        for (int i = 0; i < 10; i++) {
            if (hologram.size() > 2) {
                hologram.removeLine(2);
            }
        }

        var listConfig = (List<PlayerScore>) getConfig().getList(gameMode + "." + type);
        if (listConfig != null) {
            listConfig.stream()
                    .sorted(PlayerScore::compareTo)
                    .forEach(playerScore -> {
                        if (count.get() < 10) {
                            var line = count.incrementAndGet();
                            hologram.insertTextLine(line + 1, color + "" + line + ". " + ChatColor.AQUA + playerScore.getPlayer() + " " + ChatColor.DARK_PURPLE + playerScore.getScore());
                        }
                    });
        }
    }

    @Override
    public void onDisable() {
        gameActive = false;
        getScheduler().cancelTasks(this);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (label.equalsIgnoreCase("start") && !gameActive) {
            if (args.length >= 1) {
                switch (args[0]) {
                    case TO_EASY:
                        currentMode = TO_EASY;
                        minutes = 7;
                        break;
                    case EASY:
                        currentMode = EASY;
                        minutes = 7;
                        break;
                    case NORMAL:
                        currentMode = NORMAL;
                        minutes = 5;
                        break;
                    case HARD:
                        currentMode = HARD;
                        minutes = 5;
                        break;
                    default:
                        sender.sendMessage(ChatColor.RED + args[0] + " Is not a valid gamemode");
                        return false;
                }
            }
            stopGame = false;
            startGame();
        }
        if (label.equalsIgnoreCase("stop") && gameActive && !stopGame) {
            stopGame = true;
            getServer().broadcastMessage("Game will be stopped after this round.");
        }
        if (label.equalsIgnoreCase("rockets")) {
            ((Player) sender).getInventory().addItem(new ItemStack(Material.FIREWORK_ROCKET, 64));
        }
        return false;
    }

    private void startGame() {
        if (getOnlinePlayers().size() <= 1) {
            broadcastMessage("There are not at least 2 players, this is needed to start the game");
            return;
        }
        if (stopGame) {
            return;
        }
        startGameTimer();

        getServer().broadcastMessage("=======================");
        getServer().broadcastMessage(ChatColor.GREEN + "The game has started! You have " + ChatColor.WHITE + minutes + " minutes" + ChatColor.GREEN + " to complete the task!");

        for (Player player : getOnlinePlayers()) {
            if (!activePlayers.contains(player)) {
                activePlayers.add(player);
            }
            Random random = new Random();
            Material material = null;
            var blocks = getBlocksOfMode(currentMode);
            while (material == null) {
                assert blocks != null;
                material = blocks.get(random.nextInt(blocks.size()));
                if (!material.isBlock() && !chosedBlocks.contains(material)) {
                    material = null;
                } else {
                    chosedBlocks.add(material);
                }
            }
            playersBlocks.put(player, material.name());
            addScoreboard(player);
            player.sendTitle(ChatColor.GREEN + "Your block is", material.name(), 10, 70, 20);
        }
        endTimeDate = LocalDateTime.now().plusMinutes(minutes);
        gameActive = true;
    }

    private void startGameTimer() {
        var taskTimeUpGame = new BukkitRunnable() {
            @Override
            public void run() {
                if (gameActive) {
                    activePlayers.stream()
                            .filter(s -> playersBlocks.get(s) == null)
                            .forEach(player -> addOneToScore("wins", player));
                    playersBlocks.clear();
                    activePlayers.clear();
                    gameActive = false;
                    getServer().getOnlinePlayers()
                            .forEach(player -> player.sendTitle(ChatColor.RED + "Times's up!", ChatColor.GREEN + "Type /start to play an other round", 10, 70, 20));
                }
            }
        };
        taskTimeUpGame.runTaskLater(this, minutes * TICKS_TO_MINUTES);
        taskId = taskTimeUpGame.getTaskId();
        var taskTimeUpGame2 = new BukkitRunnable() {
            @Override
            public void run() {
                if (gameActive) {
                    activePlayers.stream()
                            .filter(s -> playersBlocks.get(s) != null)
                            .forEach(player -> player.sendTitle(ChatColor.RED + "You have only 1 minuut left", "", 10, 50, 20));
                }
            }
        };
        taskTimeUpGame2.runTaskLater(this, (minutes - 1) * TICKS_TO_MINUTES);
        taskId2 = taskTimeUpGame2.getTaskId();
    }

    private List<Material> getBlocksOfMode(String mode) {
        switch (mode) {
            case TO_EASY:
                return BLOCKS_TO_EASY;
            case EASY:
                return Stream.of(BLOCKS_TO_EASY, BLOCKS_EASY)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
            case NORMAL:
                return Stream.of(BLOCKS_TO_EASY, BLOCKS_EASY, BLOCKS_NORMAL)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
            case HARD:
                return Stream.of(BLOCKS_TO_EASY, BLOCKS_EASY, BLOCKS_NORMAL, BLOCKS_HARD)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
        }
        return null;
    }

    private void addScoreboard(Player player) {
        val scoreboardManager = getScoreboardManager();
        assert scoreboardManager != null;
        val playerScoreboard = scoreboardManager.getNewScoreboard();
        val objective = playerScoreboard.registerNewObjective("Scoreboard", "dummy",
                translateAlternateColorCodes('&', "&a&l<< &2&l BlockShuffle " + currentMode + "&a&l>>"));
        val scoreCount = new AtomicInteger(99);
        val s1 = objective.getScore("Time left: " + getTimeLeft());
        s1.setScore(scoreCount.getAndDecrement());
        for (Player p : getOnlinePlayers()) {
            setPlayerToScoreBoard(p, objective, scoreCount);
        }

        objective.setDisplaySlot(SIDEBAR);
        player.setScoreboard(playerScoreboard);
    }

    private void setPlayerToScoreBoard(Player player, Objective objective, AtomicInteger scoreCount) {
        val blockString = playersBlocks.get(player);
        val score = objective.getScore(player.getName());
        score.setScore(scoreCount.getAndDecrement());
        if (blockString != null) {
            val score2 = objective.getScore(ChatColor.GREEN + blockString);
            score2.setScore(scoreCount.getAndDecrement());
        }
    }

    private String getTimeLeft() {
        if (gameActive) {
            long totalSeconds = ChronoUnit.SECONDS.between(LocalDateTime.now(), endTimeDate);
            long minutes = totalSeconds / 60;
            long seconds = (totalSeconds) % 60;
            return String.format("%d:%02d", minutes, seconds);
        } else {
            return "NON ACTIVE";
        }
    }

    private boolean allPlayersDone() {
        for (Player player : getOnlinePlayers()) {
            if (playersBlocks.get(player) != null) {
                return false;
            }
        }
        return true;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Location loc = event.getPlayer().getLocation().clone().subtract(0, 1, 0);
        Block b = loc.getBlock();
        Player player = event.getPlayer();
        if (gameActive) {
            val block = playersBlocks.get(player);
            if (block != null && b.getType().toString().equalsIgnoreCase(block)) {
                getServer().broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.GREEN + " is done! Their block was " + ChatColor.WHITE + playersBlocks.get(player));
                playersBlocks.remove(player);
                addOneToScore("blocks", player);
                checkPlayersDone();
            }
        }
    }

    private void checkPlayersDone() {
        if ((allPlayersDone())) {
            gameActive = false;
            getScheduler().cancelTask(taskId);
            getScheduler().cancelTask(taskId2);
            getServer().broadcastMessage(" ");
            getServer().broadcastMessage(ChatColor.GOLD + "All players has found their blocks!");
            startGame();
        }
    }

    private void addOneToScore(String mode, Player player) {
        var listConfig = (List<PlayerScore>) getConfig().getList(currentMode + "." + mode);
        if (listConfig != null) {
            var playerScore = listConfig.stream()
                    .filter(p -> p.getPlayer().equals(player.getName()))
                    .findFirst().orElse(new PlayerScore(player.getName(), 0));
            playerScore.addOneToScore();
            listConfig.remove(playerScore);
            listConfig.add(playerScore);
        } else {
            listConfig = new ArrayList<>();
            listConfig.add(new PlayerScore(player.getName(), 1));
        }
        getConfig().set(currentMode + "." + mode, listConfig);
        saveConfig();
        updateAllHologrameOfGamemode(currentMode);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent playerJoinEvent) {
        startScoreboard(playerJoinEvent.getPlayer());
        if (gameActive) {
            playerJoinEvent.getPlayer().sendMessage("Welcome, there is a game going on you will be added to the game when all the blocks are found, or if the time runs out");
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent playerQuitEvent) {
        stopScoreboard(playerQuitEvent.getPlayer());
        playersBlocks.remove(playerQuitEvent.getPlayer());
        activePlayers.remove(playerQuitEvent.getPlayer());
        if ((allPlayersDone())) {
            gameActive = false;
            getScheduler().cancelTask(taskId);
            getScheduler().cancelTask(taskId2);
        }
    }

    private void stopScoreboard(Player player) {
        val lobbyBoard = new LobbyBoard(player.getUniqueId());
        if (lobbyBoard.hasID()) {
            lobbyBoard.stop();
        }
    }

    private void startScoreboard(Player player) {
        taskIdPlayerScoreboard = getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            final LobbyBoard board = new LobbyBoard(player.getUniqueId());

            @Override
            public void run() {
                if (!board.hasID()) {
                    board.setID(taskIdPlayerScoreboard);
                }
                addScoreboard(player);
            }
        }, 0, 20);
    }
}
